cmake_minimum_required(VERSION 3.15)
project(sharedptr)

set(CMAKE_CXX_STANDARD 20)

add_executable(sharedptr main.cpp)