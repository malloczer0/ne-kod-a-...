#include <iostream>
#include "boost/asio.hpp"
#include "boost/thread.hpp"

class AsioServer {
public:
    AsioServer();
    ~AsioServer();
private:
    void clientSession(boost::shared_ptr<boost::asio::ip::tcp::socket> socketPtr);
};
AsioServer::AsioServer() {
    using namespace boost::asio;
    io_service ioService;
    ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), 1488);
    ip::tcp::acceptor acceptor(ioService, endpoint);
    while (acceptor.is_open()) {
        boost::shared_ptr<boost::asio::ip::tcp::socket> socketPtr(new ip::tcp::socket(ioService));
        acceptor.accept(*socketPtr);
        //boost::thread(boost::bind(&asioServer::clientSession, socketPtr));
    }
}
AsioServer::~AsioServer() = default;
void AsioServer::clientSession(boost::shared_ptr<boost::asio::ip::tcp::socket> socketPtr) {
    while (true) {
        //std::string context;
        char context[512];
        size_t contentLength = socketPtr->read_some(boost::asio::buffer(context));
        if(contentLength != 0) {
            boost::asio::write(*socketPtr, boost::asio::buffer("OK", 2));
        }
    }
}

int main() {
    AsioServer();
    return 0;
}
